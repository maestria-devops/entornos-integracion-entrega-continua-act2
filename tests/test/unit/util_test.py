import unittest
import pytest

from app import util

@pytest.mark.unit
class TestUtil(unittest.TestCase):

    @pytest.mark.description("Prueba convertir cadenas válidas a números")
    def test_convert_to_number_correct_param(self):
        # Prueba convertir cadenas que representan números válidos a enteros y flotantes
        self.assertEqual(4, util.convert_to_number("4"))
        self.assertEqual(0, util.convert_to_number("0"))
        self.assertEqual(0, util.convert_to_number("-0"))
        self.assertEqual(-1, util.convert_to_number("-1"))
        self.assertAlmostEqual(4.0, util.convert_to_number("4.0"), delta=0.0000001)
        self.assertAlmostEqual(0.0, util.convert_to_number("0.0"), delta=0.0000001)
        self.assertAlmostEqual(0.0, util.convert_to_number("-0.0"), delta=0.0000001)
        self.assertAlmostEqual(-1.0, util.convert_to_number("-1.0"), delta=0.0000001)

    @pytest.mark.description("Prueba convertir cadenas no válidas a números")
    def test_convert_to_number_invalid_type(self):
        # Prueba que convertir cadenas que no representan números lance una excepción TypeError
        self.assertRaises(TypeError, util.convert_to_number, "")
        self.assertRaises(TypeError, util.convert_to_number, "3.h")
        self.assertRaises(TypeError, util.convert_to_number, "s")
        self.assertRaises(TypeError, util.convert_to_number, None)
        self.assertRaises(TypeError, util.convert_to_number, object())

    @pytest.mark.description("Prueba convertir cadenas válidas a números usando InvalidConvertToNumber")
    def test_InvalidConvertToNumber_correct_param(self):
        # Prueba convertir cadenas que representan números válidos a enteros y flotantes usando InvalidConvertToNumber
        self.assertEqual(4, util.InvalidConvertToNumber("4"))
        self.assertEqual(0, util.InvalidConvertToNumber("0"))
        self.assertEqual(0, util.InvalidConvertToNumber("-0"))
        self.assertEqual(-1, util.InvalidConvertToNumber("-1"))
        self.assertAlmostEqual(4.0, util.InvalidConvertToNumber("4.0"), delta=0.0000001)
        self.assertAlmostEqual(0.0, util.InvalidConvertToNumber("0.0"), delta=0.0000001)
        self.assertAlmostEqual(0.0, util.InvalidConvertToNumber("-0.0"), delta=0.0000001)
        self.assertAlmostEqual(-1.0, util.InvalidConvertToNumber("-1.0"), delta=0.0000001)

    @pytest.mark.description("Prueba convertir cadenas no válidas a números usando InvalidConvertToNumber")
    def test_InvalidConvertToNumber_invalid_type(self):
        # Prueba que convertir cadenas que no representan números lance una excepción TypeError usando InvalidConvertToNumber
        self.assertRaises(TypeError, util.InvalidConvertToNumber, "")
        self.assertRaises(TypeError, util.InvalidConvertToNumber, "3.h")
        self.assertRaises(TypeError, util.InvalidConvertToNumber, "s")
        self.assertRaises(TypeError, util.InvalidConvertToNumber, None)
        self.assertRaises(TypeError, util.InvalidConvertToNumber, object())

    @pytest.mark.description("Prueba la validación de permisos")
    def test_validate_permissions(self):
        # Prueba que validate_permissions retorne True para "user1" y False para otros usuarios
        self.assertTrue(util.validate_permissions("operation", "user1"))
        self.assertFalse(util.validate_permissions("operation", "user2"))
        self.assertFalse(util.validate_permissions("operation", "admin"))
        self.assertFalse(util.validate_permissions("operation", ""))
