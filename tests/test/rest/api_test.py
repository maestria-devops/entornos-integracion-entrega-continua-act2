import http.client
import os
import unittest
from urllib.request import urlopen
from urllib.error import HTTPError

import pytest

BASE_URL = os.environ.get("BASE_URL")
DEFAULT_TIMEOUT = 2  # in secs

@pytest.mark.api
class TestApi(unittest.TestCase):
    def setUp(self):
        # Verifica que la URL base esté configurada
        self.assertIsNotNone(BASE_URL, "URL no configurada")
        self.assertTrue(len(BASE_URL) > 8, "URL no configurada")

    @pytest.mark.description("Prueba la operación de suma en la API")
    def test_api_add(self):
        url = f"{BASE_URL}/calc/add/2/2"
        response = urlopen(url, timeout=DEFAULT_TIMEOUT)
        self.assertEqual(
            response.status, http.client.OK, f"Error en la petición API a {url}"
        )
        self.assertEqual(response.read().decode(), "4")

    @pytest.mark.description("Prueba la operación de resta en la API")
    def test_api_substract(self):
        url = f"{BASE_URL}/calc/substract/5/3"
        response = urlopen(url, timeout=DEFAULT_TIMEOUT)
        self.assertEqual(
            response.status, http.client.OK, f"Error en la petición API a {url}"
        )
        self.assertEqual(response.read().decode(), "2")

    @pytest.mark.description("Prueba la operación de multiplicación en la API")
    def test_api_multiply(self):
        url = f"{BASE_URL}/calc/multiply/3/3"
        response = urlopen(url, timeout=DEFAULT_TIMEOUT)
        self.assertEqual(
            response.status, http.client.OK, f"Error en la petición API a {url}"
        )
        self.assertEqual(response.read().decode(), "9")

    @pytest.mark.description("Prueba la operación de división en la API")
    def test_api_divide(self):
        url = f"{BASE_URL}/calc/divide/10/2"
        response = urlopen(url, timeout=DEFAULT_TIMEOUT)
        self.assertEqual(
            response.status, http.client.OK, f"Error en la petición API a {url}"
        )
        self.assertEqual(response.read().decode(), "5.0")

    @pytest.mark.description("Prueba la operación de potencia en la API")
    def test_api_power(self):
        url = f"{BASE_URL}/calc/power/2/3"
        response = urlopen(url, timeout=DEFAULT_TIMEOUT)
        self.assertEqual(
            response.status, http.client.OK, f"Error en la petición API a {url}"
        )
        self.assertEqual(response.read().decode(), "8")

    @pytest.mark.description("Prueba la operación de raíz cuadrada en la API")
    def test_api_sqrt(self):
        url = f"{BASE_URL}/calc/sqrt/16"
        response = urlopen(url, timeout=DEFAULT_TIMEOUT)
        self.assertEqual(
            response.status, http.client.OK, f"Error en la petición API a {url}"
        )
        self.assertEqual(response.read().decode(), "4.0")

    @pytest.mark.description("Prueba la operación de logaritmo base 10 en la API")
    def test_api_log10(self):
        url = f"{BASE_URL}/calc/log10/1000"
        response = urlopen(url, timeout=DEFAULT_TIMEOUT)
        self.assertEqual(
            response.status, http.client.OK, f"Error en la petición API a {url}"
        )
        self.assertEqual(response.read().decode(), "3.0")

    @pytest.mark.description("Prueba la operación de suma con un parámetro inválido en la API")
    def test_api_invalid_add(self):
        url = f"{BASE_URL}/calc/add/2/invalid"
        with self.assertRaises(HTTPError) as cm:
            urlopen(url, timeout=DEFAULT_TIMEOUT)
        self.assertEqual(cm.exception.code, http.client.BAD_REQUEST)

    @pytest.mark.description("Prueba la operación de resta con un parámetro inválido en la API")
    def test_api_invalid_substract(self):
        url = f"{BASE_URL}/calc/substract/invalid/3"
        with self.assertRaises(HTTPError) as cm:
            urlopen(url, timeout=DEFAULT_TIMEOUT)
        self.assertEqual(cm.exception.code, http.client.BAD_REQUEST)

    @pytest.mark.description("Prueba la operación de multiplicación con un parámetro inválido en la API")
    def test_api_invalid_multiply(self):
        url = f"{BASE_URL}/calc/multiply/invalid/3"
        with self.assertRaises(HTTPError) as cm:
            urlopen(url, timeout=DEFAULT_TIMEOUT)
        self.assertEqual(cm.exception.code, http.client.BAD_REQUEST)

    @pytest.mark.description("Prueba la operación de división con un parámetro inválido en la API")
    def test_api_invalid_divide(self):
        url = f"{BASE_URL}/calc/divide/10/invalid"
        with self.assertRaises(HTTPError) as cm:
            urlopen(url, timeout=DEFAULT_TIMEOUT)
        self.assertEqual(cm.exception.code, http.client.BAD_REQUEST)

    @pytest.mark.description("Prueba la operación de división por cero en la API")
    def test_api_divide_by_zero(self):
        url = f"{BASE_URL}/calc/divide/10/0"
        with self.assertRaises(HTTPError) as cm:
            urlopen(url, timeout=DEFAULT_TIMEOUT)
        self.assertEqual(cm.exception.code, http.client.BAD_REQUEST)

    @pytest.mark.description("Prueba la operación de potencia con un parámetro inválido en la API")
    def test_api_invalid_power(self):
        url = f"{BASE_URL}/calc/power/2/invalid"
        with self.assertRaises(HTTPError) as cm:
            urlopen(url, timeout=DEFAULT_TIMEOUT)
        self.assertEqual(cm.exception.code, http.client.BAD_REQUEST)

    @pytest.mark.description("Prueba la operación de raíz cuadrada con un parámetro inválido en la API")
    def test_api_invalid_sqrt(self):
        url = f"{BASE_URL}/calc/sqrt/invalid"
        with self.assertRaises(HTTPError) as cm:
            urlopen(url, timeout=DEFAULT_TIMEOUT)
        self.assertEqual(cm.exception.code, http.client.BAD_REQUEST)

    @pytest.mark.description("Prueba la operación de raíz cuadrada de un número negativo en la API")
    def test_api_negative_sqrt(self):
        url = f"{BASE_URL}/calc/sqrt/-16"
        with self.assertRaises(HTTPError) as cm:
            urlopen(url, timeout=DEFAULT_TIMEOUT)
        self.assertEqual(cm.exception.code, http.client.BAD_REQUEST)

    @pytest.mark.description("Prueba la operación de logaritmo base 10 con un parámetro inválido en la API")
    def test_api_invalid_log10(self):
        url = f"{BASE_URL}/calc/log10/invalid"
        with self.assertRaises(HTTPError) as cm:
            urlopen(url, timeout=DEFAULT_TIMEOUT)
        self.assertEqual(cm.exception.code, http.client.BAD_REQUEST)

    @pytest.mark.description("Prueba la operación de logaritmo base 10 con un número no positivo en la API")
    def test_api_non_positive_log10(self):
        url = f"{BASE_URL}/calc/log10/0"
        with self.assertRaises(HTTPError) as cm:
            urlopen(url, timeout=DEFAULT_TIMEOUT)
        self.assertEqual(cm.exception.code, http.client.BAD_REQUEST)

        url = f"{BASE_URL}/calc/log10/-100"
        with self.assertRaises(HTTPError) as cm:
            urlopen(url, timeout=DEFAULT_TIMEOUT)
        self.assertEqual(cm.exception.code, http.client.BAD_REQUEST)