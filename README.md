# Vagrant VM Workstation with Docker (For phyton Tests)

This Workstation has installed:
- Google Chrome
- Docker (Container Technology)

## Requirements
- [Oracle Virtualbox](https://www.virtualbox.org/)
- [Vagrant](https://www.vagrantup.com/)
- Minimum 8 GB RAM

## First create VM
`vagrant up`

if your keyboard is LATAM open a terminal and write: `. .profile`

## Stops for build samples
Open a terminal and execurte the nexts steps:
1. Build Python Calculator App: `make build`
2. Execute Unit Tests: `make test-unit`
3. Execute API Tests: `make test-api`
4. Execute End to End Tests: `make test-e2e`

## The test results are about:
1. Unit Tests and Coverage: 
   1. $HOME/tests/results/unit_result.html
   2. $HOME/tests/results/coverage.html
2. API Tests:
   1. $HOME/tests/results/api_result.html
3. End to End Tests:
   1. $HOME/tests/results/cypress_result.html



